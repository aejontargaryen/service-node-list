# DRGL service-nodes list
### This list will be used for remote connection options inside DRGL wallets.
#### To include any stable or service-node in this list just click the edit button and add pull request
###### (add the``--fee-address=``dæmon flag followed by your DRGL address, when starting your dæmon if you wish to earn txn fees)
###### <sup>((example ``--fee-address=dRGLbfa8qaHZLKAuENnjtxAsahDWAmoFJXf9GpR9J78Jji6uBAwLzXM8ynT9ALZ5q8SGV86d162RLdhZRvuTbKsq8Mhys1aVgd``))</sup>

--------------------------

-  ice.zirtysperzys.info:10818
-  fire.zirtysperzys.online:10818
-  mine.drgl.online:10818
